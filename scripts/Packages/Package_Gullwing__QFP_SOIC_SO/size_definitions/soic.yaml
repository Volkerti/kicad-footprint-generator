# SOIC - small outline integrated circuit
#
# This file cointains JEDEC SOIC and JEITA/EIAJ P-SOP (or simplifying SOP) packages with lead space / pitch 1.27 mm
#
# Here some definitions of a JEDEC SOIC package (see JEP95 from JEDEC JC-11 committee for more info):
#
# standard  | width                                             | height (max)     | leads (N)                   | PS
# MS-012    | 150 mil (3.81 mm) / 3.75 mm / 3.90 mm (de facto)  | 1.75 mm          | 8, 14, 16                   |
# MS-013    | 300 mil (7.62 mm) / 7.50 mm                       | 2.65 mm          | 8, 14, 16, 18, 20, 24, 28   | has thermal variations
# MO-046    | 200 mil (5.08 mm) / 5.30 mm                       | 1.80 mm          | 14, 16, 20                  |
# MO-059    | 8.4 mm (330 mil)                                  | 2.54 / 3.05 mm   | 24, 28                      |
# MO-099    | 11.2 mm (440 mil)                                 | 3.04 mm          | 28, 32                      |
# MO-119    | 300 mil (7.62 mm)                                 | 2.64 mm          | 24, 28, 32, 36              |
# MO-120    | 350 mil (8.89 mm)                                 | 2.64 mm          | 24, 28, 32, 36              |
# MO-155    | 4.40 mm                                           | 3.00 mm          | 5                           |
# MO-175    | 12.60 mm                                          | 3.10 mm          | 44, 48                      |
# MO-180    | 13.30 mm                                          | 2.80 mm          | 44                          | there are two other packages with pitch 0.80 and 0.50 mm, please do not add them to this file
#
# For more info about JEITA/EIAJ SOP packages, see ED-7311-19 and EDR-7320 standards


FileHeader:
  library_Suffix: 'SO'
  device_type: 'SOIC'

SOIC-4_4.55x3.7mm_P2.54mm:
  custom_name_format: 'SOIC-4_4.55x3.7mm_P2.54mm' #does not take into account the excluded pins
  size_source: 'https://toshiba.semicon-storage.com/info/docget.jsp?did=11791&prodName=TLP185'
  body_size_x:
    minimum: 4.4
    nominal: 4.55
    maximum: 4.8
  body_size_y:
    minimum: 3.45
    nominal: 3.7
    maximum: 3.95
  overall_size_x:
    minimum: 6.6
    nominal: 7.0
    maximum: 7.4
  lead_width:
    nominal: 0.4
  lead_len:
    nominal: 0.5
  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 3
  hidden_pins: [2, 5]

SOIC-4_4.55x2.6mm_P1.27mm:
  size_source: 'https://toshiba.semicon-storage.com/info/docget.jsp?did=12884&prodName=TLP291'
  body_size_x:
    minimum: 4.4
    nominal: 4.55
    maximum: 4.8
  body_size_y:
    minimum: 2.45
    nominal: 2.6
    maximum: 2.85
  overall_size_x:
    minimum: 6.6
    nominal: 7.0
    maximum: 7.4
  lead_width:
    minimum: 0.37
    maximum: 0.39
  lead_len:
    nominal: 0.5
  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 2

SOIC-8_3.9x4.9mm_P1.27mm:
  size_source: 'JEDEC MS-012AA, https://www.analog.com/media/en/package-pcb-resources/package/pkg_pdf/soic_narrow-r/r_8.pdf'
  body_size_x: # from JEDEC
    nominal: 3.9
    tolerance: 0.1
  body_size_y:
    minimum: 4.8
    maximum: 5.0
  overall_height:
    minimum: 1.35
    maximum: 1.75

  overall_size_x: # from JEDEC (agrees with linked datasheet)
    nominal: 6
    tolerance: 0.2
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width: # from JEDEC (agrees with linked datasheet)
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 4

SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.29x3.0mm:
  size_source: 'https://www.analog.com/media/en/technical-documentation/data-sheets/ada4898-1_4898-2.pdf#page=29'
  body_size_x:
    minimum: 3.8
    nominal: 3.9
    maximum: 4.0
  body_size_y:
    minimum: 4.8
    nominal: 4.9
    maximum: 5.0
  overall_height:
    minimum: 1.25
    maximum: 1.65

  overall_size_x:
    minimum: 5.8
    nominal: 6.0
    maximum: 6.2
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width:
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 4

  EP_size_x:
    nominal: 2.29
    tolerance: 0 # no tolerance given
  #EP_size_x_overwrite:
  #EP_mask_x:

  EP_size_y:
    nominal: 3.0 # 2.29+2*0.356 = 3.002 rounded to 3.0
    tolerance: 0 # no tolerance given
  #EP_size_y_overwrite:
  #EP_mask_y:

  # EP_paste_coverage: 0.65
  EP_num_paste_pads: [2, 2]

  thermal_vias:
    count: [2, 3]
    drill: 0.2
    # min_annular_ring: 0.15
    paste_via_clearance: 0.1
    EP_num_paste_pads: [2, 2]
    # paste_between_vias: 1
    # paste_rings_outside: 1
    EP_paste_coverage: 0.7
    grid: [1.3, 1]
    # bottom_pad_size:
    paste_avoid_via: False

SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.41x3.81mm:
  size_source: 'https://www.analog.com/media/en/technical-documentation/data-sheets/ada4898-1_4898-2.pdf#page=29'
  body_size_x:
    minimum: 3.8
    nominal: 3.9
    maximum: 4.0
  body_size_y:
    minimum: 4.8
    nominal: 4.9
    maximum: 5.0
  overall_height:
    minimum: 1.25
    maximum: 1.65

  overall_size_x:
    minimum: 5.8
    nominal: 6.0
    maximum: 6.2
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width:
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 4

  EP_size_x:
    nominal: 2.41
    tolerance: 0 # no tolerance given
  # EP_size_x_overwrite: 2.41
  #EP_mask_x:

  EP_size_y:
    nominal: 3.81 # 3.098+2*0.356
    tolerance: 0 # no tolerance given
  # EP_size_y_overwrite: 3.1
  #EP_mask_y:

  # EP_paste_coverage: 0.65
  EP_num_paste_pads: [2, 2]

  thermal_vias:
    count: [2, 3]
    drill: 0.2
    # min_annular_ring: 0.15
    paste_via_clearance: 0.1
    EP_num_paste_pads: [2, 2]
    # paste_between_vias: 1
    # paste_rings_outside: 1
    EP_paste_coverage: 0.7
    grid: [1.4, 1.4]
    # bottom_pad_size:
    paste_avoid_via: False

SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.41x3.1mm:
  size_source: 'http://www.ti.com/lit/ds/symlink/lm5017.pdf#page=31'
  body_size_x:
    minimum: 3.8
    maximum: 4.0
  body_size_y:
    minimum: 4.8
    maximum: 5.0
  overall_height:
    maximum: 1.7

  overall_size_x:
    minimum: 5.8
    maximum: 6.2
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width:
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 4

  EP_size_x:
    minimum: 2.11
    maximum: 2.71
  EP_size_x_overwrite: 2.95
  EP_mask_x: 2.71

  EP_size_y:
    minimum: 2.8
    maximum: 3.4
  EP_size_y_overwrite: 4.9
  EP_mask_y: 3.4

  # EP_paste_coverage: 0.65
  EP_num_paste_pads: [2, 2]

  thermal_vias:
    count: [2, 4]
    drill: 0.2
    # min_annular_ring: 0.15
    paste_via_clearance: 0.1
    EP_num_paste_pads: [2, 2]
    # paste_between_vias: 1
    # paste_rings_outside: 1
    EP_paste_coverage: 0.7
    grid: [1.3, 1.3]
    # bottom_pad_size:
    paste_avoid_via: False

SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.41x3.3mm:
  size_source: 'http://www.allegromicro.com/~/media/Files/Datasheets/A4950-Datasheet.ashx#page=8'
  body_size_x:
    nominal: 3.9
    tolerance: 0.1
  body_size_y:
    nominal: 4.9
    tolerance: 0.1
  overall_height:
    maximum: 1.7

  overall_size_x:
    nominal: 6.0
    tolerance: 0.2
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width:
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 4

  EP_size_x:
    nominal: 2.41
    tolerance: 0 # no tolerance given
  # EP_size_x_overwrite: 2.41
  #EP_mask_x:

  EP_size_y:
    nominal: 3.3
    tolerance: 0 # no tolerance given
  # EP_size_y_overwrite: 3.1
  #EP_mask_y:

  # EP_paste_coverage: 0.65
  EP_num_paste_pads: [2, 2]

  thermal_vias:
    count: [2, 3]
    drill: 0.2
    # min_annular_ring: 0.15
    paste_via_clearance: 0.1
    EP_num_paste_pads: [2, 2]
    # paste_between_vias: 1
    # paste_rings_outside: 1
    EP_paste_coverage: 0.7
    grid: [1.4, 1.2]
    # bottom_pad_size:
    paste_avoid_via: False

SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.514x3.2mm:
  size_source: 'https://www.renesas.com/eu/en/www/doc/datasheet/hip2100.pdf#page=13'
  body_size_x:
    minimum: 3.8 # rounded from 3.811
    maximum: 4.0 # rounded from 3.99
  body_size_y:
    minimum: 4.8
    maximum: 5.0 # rounded from 4.98
  overall_height:
    minimum: 1.43
    maximum: 1.68

  overall_size_x:
    minimum: 5.84
    maximum: 6.2
  lead_len:
    minimum: 0.41
    maximum: 0.89
  lead_width:
    minimum: 0.35
    maximum: 0.49

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 4

  EP_size_x:
    nominal: 2.514 # only maximum given but that is not supported by the script
    tolerance: 0
  # EP_size_x_overwrite:
  #EP_mask_x:

  EP_size_y:
    nominal: 3.2 # only maximum given but that is not supported by the script
    tolerance: 0
  # EP_size_y_overwrite:
  #EP_mask_y:

  # EP_paste_coverage: 0.65
  EP_num_paste_pads: [2, 2]

  thermal_vias:
    count: [2, 3]
    drill: 0.2
    # min_annular_ring: 0.15
    paste_via_clearance: 0.1
    EP_num_paste_pads: [2, 2]
    # paste_between_vias: 1
    # paste_rings_outside: 1
    EP_paste_coverage: 0.7
    grid: [1.4, 1.0]
    # bottom_pad_size:
    paste_avoid_via: False



SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.62x3.51mm:
  size_source: 'https://www.monolithicpower.com/en/documentview/productdocument/index/version/2/document_type/Datasheet/lang/en/sku/MP2303A/document_id/494#page=14'
  body_size_x:
    minimum: 3.8
    maximum: 4.0
  body_size_y:
    minimum: 4.8
    maximum: 5.0
  overall_height:
    minimum: 1.3
    maximum: 1.7

  overall_size_x:
    minimum: 5.8
    maximum: 6.2
  lead_len:
    minimum: 0.41
    maximum: 1.27
  lead_width:
    minimum: 0.33
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 4

  EP_size_x:
    minimum: 2.26
    maximum: 2.56

  EP_size_y:
    minimum: 3.15
    maximum: 3.45

  EP_size_x_overwrite: 2.62
  EP_size_y_overwrite: 3.51

  # EP_paste_coverage: 0.65
  EP_num_paste_pads: [2, 2]

  thermal_vias:
    count: [2, 3]
    drill: 0.2
    # min_annular_ring: 0.15
    paste_via_clearance: 0.1
    EP_num_paste_pads: [2, 2]
    # paste_between_vias: 1
    # paste_rings_outside: 1
    EP_paste_coverage: 0.7
    grid: [1.4, 1.0]
    # bottom_pad_size:
    paste_avoid_via: False


SOIC-8_5.3x5.3mm_P1.27mm:
  # formerly SOIC-8W_5.3x5.3mm_P1.27mm, 208 mils width, we are removing 'W' since it does not make sense in this package width
  # it replaces:
  # - SOP-8_5.28x5.23mm_P1.27mm
  # - SOIC-8_5.23x5.23mm_P1.27mm
  # - SOIC-8_5.275x5.275mm_P1.27mm
  # - SOIC-8W_5.3x5.3mm_P1.27mm
  #
  # based on JEITA/EIAJ 08-001-BBA + Atmel/Microchip + others manufacturers
  size_source: 'JEITA/EIAJ 08-001-BBA and Atmel/Microchip, 208 mils width, https://www.jeita.or.jp/japanese/standard/book/ED-7311-19/#target/page_no=21, https://ww1.microchip.com/downloads/en/DeviceDoc/20005045C.pdf#page=23, https://ww1.microchip.com/downloads/en/DeviceDoc/doc2535.pdf#page=162'
  compatible_mpns:
    - 'P-SOP SOP SOP-8'
    - 'SO SO-8'
    - '8S2'              # Atmel/Microchip, see link above
    - 'S2AE/F'           # Microchip, https://ww1.microchip.com/downloads/aemDocuments/documents/corporate-responsibilty/environmental/material-compliance-documents/8L_SOIC_point208inch_S2AE_F_C0414500A.pdf
    - 'K04-056'          # Microchip, https://ww1.microchip.com/downloads/en/devicedoc/40139e.pdf#page=102
    - 'CASE-751BE'       # Onsemi, https://www.onsemi.com/pdf/datasheet/cat24m01-d.pdf#page=12
    - 'SO8W'             # Micron, https://www.mouser.de/datasheet/2/671/M25PX32-1282938.pdf#page=59
    - '8-Pin-SOIC'       # Winbond, https://www.winbond.com/resource-files/w25q32jv%20revg%2003272018%20plus.pdf#page=68
    - 'PSA'              # Texas, https://www.ti.com/lit/ml/mpds329/mpds329.pdf, https://www.ti.com/lit/ds/symlink/amc1203.pdf#page=29
    - 'W8-2 W8-4 W8MS-1' # Maxim/Analog, https://www.analog.com/media/en/package-pcb-resources/package/pkg_pdf/soic/21-0262.pdf
  body_size_x: 5.15 .. 5.30 .. 5.40 # 5.30mm from JEITA
  body_size_y: 5.15 .. 5.30 .. 5.40 # 5.30mm nominal is good for all manufacturers sources used here, but "out of the standard" in JEITA, same value of old SOIC-8W
  overall_size_x: 7.70 .. 7.80 .. 8.10 # minimum / maximum from manufacturers, nominal from JEITA
  overall_height: 1.75 .. 2.16 # JEITA is 2.15mm, manufacturers use 2.16mm
  lead_width: 0.35 .. 0.55  # JEITA bp
  lead_len: 0.45 .. 0.85 # JEITA Lp minimum value .. Atmel '8S2' L maximum value
  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 4

#SOP-8_5.28x5.23mm_P1.27mm:
#  # originaly it was in sop.yaml
#  # old link, 'http://www.macronix.com/Lists/Datasheet/Attachments/7534/MX25R3235F,%20Wide%20Range,%2032Mb,%20v1.6.pdf#page=80'
#  # they call SOP-8, 'official name - 209MIL' which is = 5.308mm
#  # second source, https://www.macronix.com/Lists/ApplicationNote/Attachments/1886/AN182V1-209mil%208-SOP%20and%206x5mm%208-WSON%20Dual%20Layout%20Guide.pdf
#  size_source: 'https://web.archive.org/web/20190226212332/http://www.macronix.com/Lists/Datasheet/Attachments/7534/MX25R3235F,%20Wide%20Range,%2032Mb,%20v1.6.pdf#page=80'
#  body_size_x: 5.18 .. 5.28 .. 5.38
#  body_size_y: 5.13 .. 5.23 .. 5.33
#  overall_height: 1.75 .. 1.95 .. 2.16
#  overall_size_x: 7.70 .. 7.90 .. 8.10
#  lead_width: 0.36 .. 0.41 .. 0.51
#  lead_len: 0.50 .. 0.65 .. 0.80

#SOP-8_5.29x5.24mm_P1.27mm:
#  # this one is just for reference (never existed in the library),
#  # many ATtiny in lib use Package_SO:SOIC-8W_5.3x5.3mm_P1.27mm, manually created, its source is https://ww1.microchip.com/downloads/en/PackagingSpec/00000049BQ.pdf#page=174
#  # but then on their datasheet you can find Atmel 8S2 package, 208 mils == 5.283 mm
#  size_source: 'https://ww1.microchip.com/downloads/en/DeviceDoc/doc2535.pdf#page=162'
#  # other source that mentions '8S2', here it's 210 mils == 5.334 mm and has different measures, older? https://datasheet.octopart.com/AT86RF233-ZU-Atmel-datasheet-22122754.pdf#page=27
#  compatible_mpns:
#    - '8S2'
#  body_size_x: 5.18 .. 5.40
#  body_size_y: 5.13 .. 5.35
#  overall_height: 1.70 .. 2.16
#  overall_size_x: 7.70 .. 8.26
#  lead_width: 0.35 .. 0.48
#  lead_len: 0.51 .. 0.85

#SOIC-8_5.28x5.28mm_P1.27mm:
#  # formerly SOIC-8_5.23x5.23mm_P1.27mm (it was using E1 and D1 by mistake in name), replaced by/see: SOIC-8_5.3x5.3mm_P1.27mm
#  size_source: 'http://www.winbond.com/resource-files/w25q32jv%20revg%2003272018%20plus.pdf#page=68'
#  body_size_x: 5.18 .. 5.28 .. 5.38 # E
#  body_size_y: 5.18 .. 5.28 .. 5.38 # D
#  overall_height: 1.75 .. 1.95 .. 2.16
#  overall_size_x: 7.7 .. 7.9 .. 8.1
#  lead_len: 0.5 .. 0.65 .. 0.8
#  lead_width: 0.35 .. 0.42 .. 0.48

#SOIC-8_5.275x5.275mm_P1.27mm:
#  # replaced by/see: SOIC-8_5.3x5.3mm_P1.27mm
#  size_source: 'http://ww1.microchip.com/downloads/en/DeviceDoc/20005045C.pdf#page=23'
#  body_size_x: 5.15 .. 5.4
#  body_size_y: 5.15 .. 5.4
#  overall_size_x: 7.7 .. 8.1
#  overall_height: 1.75 .. 2.16
#  lead_width: 0.35 .. 0.5
#  lead_len: 0.5 .. 0.8

SOIC-8_5.3x6.2mm_P1.27mm:
  # formerly SO-8_5.3x6.2mm_P1.27mm
  # similar to JEITA/EIAJ 08-001-BBA, which is E=5.30mm, D=6.10mm, A=2.15mm
  # see https://www.ti.com/packaging/docs/searchtipackages.tsp?packageName=SO, they call this package SOP
  size_source: 'Texas Instruments (based on JEITA/EIAJ 08-001-BBA), 208 mils width, https://www.ti.com/lit/ml/msop001a/msop001a.pdf'
  compatible_mpns:
    - 'P-SOP SOP SOP-8'
    - 'SO SO-8'
    - 'PS' # Texas, https://www.ti.com/lit/ds/symlink/lm2904.pdf#page=61
  body_size_x: 5.00 .. 5.60
  body_size_y: 5.90 .. 6.50
  overall_height: 2.00
  overall_size_x: 7.40 .. 8.20
  lead_width: 0.35 .. 0.51
  lead_len: 0.55 .. 0.95 # JEITA Lp is maximum 0.75
  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 4

SOIC-8_7.5x5.85mm_P1.27mm:
  size_source: 'http://www.ti.com/lit/ml/mpds382b/mpds382b.pdf'
  body_size_x:
    minimum: 7.4
    maximum: 7.6
  body_size_y:
    minimum: 5.75
    maximum: 5.95
  overall_height:
    maximum: 2.8
  overall_size_x:
    nominal: 11.5
    tolerance: 0.25
  lead_len:
    minimum: 0.5
    maximum: 1.0
  lead_width:
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 4

SOIC-10_3.9x4.9mm_P1.00mm:
  # it's probably SSOP, but out of standard
  custom_name_format: SOIC-{pincount}_{size_x:.2g}x{size_y:.2g}mm_P{pitch:.3g}mm
  size_source: 'https://www.onsemi.com/pub/Collateral/751BQ.PDF'
  additional_tags:
  - "SOIC-10 NB"
  body_size_x:
    minimum: 3.8
    maximum: 4.0
  body_size_y:
    minimum: 4.8
    maximum: 5.0
  overall_height:
    minimum: 1.35
    maximum: 2.00

  overall_size_x:
    minimum: 5.8
    maximum: 6.2
  lead_len:
    minimum: 0.4
    maximum: 0.8
  lead_width:
    minimum: 0.31
    maximum: 0.51

  pitch: 1.00
  num_pins_x: 0
  num_pins_y: 5

SOIC-14_3.9x8.7mm_P1.27mm:
  #round to two significant digits to comply with old name
  custom_name_format: SOIC-{pincount}_{size_x:.2g}x{size_y:.2g}mm_P{pitch:g}mm
  size_source: 'JEDEC MS-012AB, https://www.analog.com/media/en/package-pcb-resources/package/pkg_pdf/soic_narrow-r/r_14.pdf'
  body_size_x: # from JEDEC
    nominal: 3.9
    tolerance: 0.1
  body_size_y:
    minimum: 8.55
    maximum: 8.75
  overall_height:
    maximum: 1.75

  overall_size_x: # from JEDEC (agrees with linked datasheet)
    nominal: 6
    tolerance: 0.2
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width: # from JEDEC (agrees with linked datasheet)
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 7

SOIC-14W_7.5x9.0mm_P1.27mm:
  custom_name_format: SOIC-{pincount}W_{size_x:g}x{size_y:g}mm_P{pitch:g}mm
  size_source: 'JEDEC MS-013AF, https://www.analog.com/media/en/package-pcb-resources/package/54614177245586rw_14.pdf'
  body_size_x: # from JEDEC (agrees with linked datasheet)
    nominal: 7.5
    tolerance: 0.1
  body_size_y:
    minimum: 8.8
    maximum: 9.2
  overall_height:
    minimum: 2.35
    maximum: 2.65

  overall_size_x: # from JEDEC
    nominal: 10.3
    tolerance: 0.33
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width: # from JEDEC (agrees with linked datasheet)
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 7

SOIC-16_3.9x9.9mm_P1.27mm:
  size_source: 'JEDEC MS-012AC, https://www.analog.com/media/en/package-pcb-resources/package/pkg_pdf/soic_narrow-r/r_16.pdf'
  body_size_x: # from JEDEC
    nominal: 3.9
    tolerance: 0.1
  body_size_y:
    minimum: 9.8
    maximum: 10
  overall_height:
    maximum: 1.75

  overall_size_x: # from JEDEC (agrees with linked datasheet)
    nominal: 6
    tolerance: 0.2
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width: # from JEDEC (agrees with linked datasheet)
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 8

SOIC-16_4.55x10.3mm_P1.27mm:
  size_source: 'https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4'
  body_size_x:
    minimum: 4.4
    nominal: 4.55
    maximum: 4.8
  body_size_y:
    minimum: 10.15
    nominal: 10.3
    maximum: 10.55
  overall_size_x:
    minimum: 6.6
    nominal: 7.0
    maximum: 7.4
  lead_width:
    minimum: 0.39
    maximum: 0.41
  lead_len:
    nominal: 0.5
  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 8

SOIC-16W_5.3x10.2mm_P1.27mm:
  custom_name_format: SOIC-{pincount}W_{size_x:g}x{size_y:g}mm_P{pitch:g}mm
  # NOTE: Part: AM26LV32INS: https://www.ti.com/lit/ds/symlink/am26lv32.pdf
  # (same drawing as size_source)
  size_source: 'http://www.ti.com/lit/ml/msop002a/msop002a.pdf'
  body_size_x: # from JEDEC (agrees with linked datasheet)
    minimum: 5.0
    maximum: 5.6
  body_size_y:
    minimum: 9.9
    maximum: 10.5
  overall_height: # Datasheet only specifies maximum
    nominal: 2.0
    maximum: 2.0
  overall_size_x:
    minimum: 7.4
    maximum: 8.2
  lead_len:
    minimum: 1.05 # From overall_size_x[min] - body_size_x[nominal]
    maximum: 1.45 # From overall_size_x[max] - body_size_x[nominal]
  lead_width:
    minimum: 0.35
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 8

  compatible_mpns:
    - TI-NS
    - TI-NS0016A


SOIC-16W_7.5x10.3mm_P1.27mm:
  custom_name_format: SOIC-{pincount}W_{size_x:g}x{size_y:g}mm_P{pitch:g}mm
  size_source: 'JEDEC MS-013AA, https://www.analog.com/media/en/package-pcb-resources/package/pkg_pdf/soic_wide-rw/rw_16.pdf'
  body_size_x: # from JEDEC (agrees with linked datasheet)
    nominal: 7.5
    tolerance: 0.1
  body_size_y:
    minimum: 10.1
    maximum: 10.5
  overall_height:
    minimum: 2.35
    maximum: 2.65

  overall_size_x: # from JEDEC
    nominal: 10.3
    tolerance: 0.33
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width: # from JEDEC (agrees with linked datasheet)
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 8

SOIC-16W_7.5x12.8mm_P1.27mm:
  #round to two significant digits to comply with old name
  custom_name_format: SOIC-{pincount}W_{size_x:.2g}x{size_y:.3g}mm_P{pitch:g}mm
  size_source: 'https://www.analog.com/media/en/package-pcb-resources/package/pkg_pdf/ri_soic_ic/ri_16_1.pdf'
  body_size_x:
    minimum: 7.4
    maximum: 7.6
  body_size_y:
    minimum: 12.6
    maximum: 13
  overall_height:
    minimum: 2.35
    maximum: 2.65

  overall_size_x: # from JEDEC
    nominal: 10.3
    tolerance: 0.33
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width: # from JEDEC (agrees with linked datasheet)
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 8

SOIC-18W_7.5x11.6mm_P1.27mm:
  #round to two significant digits to comply with old name
  custom_name_format: SOIC-{pincount}W_{size_x:.2g}x{size_y:.3g}mm_P{pitch:g}mm
  size_source: 'JEDEC MS-013AB, https://www.analog.com/media/en/package-pcb-resources/package/33254132129439rw_18.pdf'
  body_size_x: # from JEDEC (agrees with linked datasheet)
    nominal: 7.5
    tolerance: 0.1
  body_size_y:
    minimum: 11.35
    maximum: 11.75
  overall_height:
    minimum: 2.35
    maximum: 2.65

  overall_size_x: # from JEDEC
    nominal: 10.3
    tolerance: 0.33
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width: # from JEDEC (agrees with linked datasheet)
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 9

SOIC-20W_7.5x12.8mm_P1.27mm:
  #round to two significant digits to comply with old name
  custom_name_format: SOIC-{pincount}W_{size_x:.2g}x{size_y:.3g}mm_P{pitch:g}mm
  size_source: 'JEDEC MS-013AC, https://www.analog.com/media/en/package-pcb-resources/package/233848rw_20.pdf'
  body_size_x: # from JEDEC (agrees with linked datasheet)
    nominal: 7.5
    tolerance: 0.1
  body_size_y:
    minimum: 12.6
    maximum: 13.0
  overall_height:
    minimum: 2.35
    maximum: 2.65

  overall_size_x: # from JEDEC
    nominal: 10.3
    tolerance: 0.33
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width: # from JEDEC (agrees with linked datasheet)
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 10

Infineon_SOIC-20W_7.6x12.8mm_P1.27mm:
  # similar to JEDEC MS-013G / variation AC, but body_size_x (E1 dimension) close to 300 mil
  # inherit: SOIC-20W_7.5x12.8mm_P1.27mm
  custom_name_format: '{man}_SOIC-{pincount}W_{size_x:.2g}x{size_y:.3g}mm_P{pitch:g}mm'
  size_source: 'https://www.infineon.com/dgdl/Infineon-PG-DSO-20-77_SPO_PDF-Package-v15_00-EN.pdf?fileId=5546d4625c54d85b015c627e1a0c23b3, https://www.infineon.com/dgdl/Infineon-PG-DSO-20-77_FPD_PDF-Footprint-v15_00-EN.pdf?fileId=5546d4625c54d85b015c5f0f49614324'
  manufacturer: 'Infineon'
  compatible_mpns:
    - 'PG-DSO-20'
    - 'PG-DSO-20-6'
    - 'PG-DSO-20-9'
    - 'PG-DSO-20-31'
    - 'PG-DSO-20-32'
    - 'PG-DSO-20-36'
    - 'PG-DSO-20-43'
    - 'PG-DSO-20-45'
    - 'PG-DSO-20-55'
    - 'PG-DSO-20-66'
    - 'PG-DSO-20-77'

  body_size_x: 7.6 +0.0 -0.2
  body_size_y: 12.8 +0.0 -0.2
  overall_height:
    maximum: 2.65
  overall_size_x: 10.3 +/-0.3

  lead_len: 0.4 +0.8 -0.0 # first source
  lead_width: 0.35 +0.15 -0.0

  pad_size_y_overwrite: 0.65 # second source, pad width
  pad_to_pad_min_x_overwrite: 8.06 # = 9.73 - 1.67
  pad_to_pad_max_x_overwrite: 11.4 # = 9.73 + 1.67, see second source

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 10

SOIC-20W_7.5x15.4mm_P1.27mm:
  #round to two significant digits to comply with old name
  custom_name_format: SOIC-{pincount}W_{size_x:.2g}x{size_y:.3g}mm_P{pitch:g}mm
  size_source: 'JEDEC MS-013AD, https://www.analog.com/media/en/package-pcb-resources/package/pkg_pdf/soic_wide-rw/RI_20_1.pdf'
  body_size_x:
    minimum: 7.39
    maximum: 7.59
  body_size_y:
    minimum: 15.27
    maximum: 15.54
  overall_height:
    minimum: 2.36
    maximum: 2.64

  overall_size_x: # from JEDEC
    nominal: 10.3
    tolerance: 0.24
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width:
    minimum: 0.36
    maximum: 0.48

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 10

SOIC-24W_7.5x15.4mm_P1.27mm:
  #round to two significant digits to comply with old name
  custom_name_format: SOIC-{pincount}W_{size_x:.2g}x{size_y:.3g}mm_P{pitch:g}mm
  size_source: 'JEDEC MS-013AD, https://www.analog.com/media/en/package-pcb-resources/package/pkg_pdf/soic_wide-rw/RW_24.pdf'
  body_size_x: # from JEDEC (agrees with linked datasheet)
    nominal: 7.5
    tolerance: 0.1
  body_size_y:
    minimum: 15.2
    maximum: 15.6
  overall_height:
    minimum: 2.35
    maximum: 2.65

  overall_size_x: # from JEDEC
    nominal: 10.3
    tolerance: 0.33
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width: # from JEDEC (agrees with linked datasheet)
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 12

SOIC-28W_7.5x17.9mm_P1.27mm:
  #round to two significant digits to comply with old name
  custom_name_format: SOIC-{pincount}W_{size_x:.2g}x{size_y:.3g}mm_P{pitch:g}mm
  size_source: 'JEDEC MS-013AE, https://www.analog.com/media/en/package-pcb-resources/package/35833120341221rw_28.pdf'
  body_size_x: # from JEDEC (agrees with linked datasheet)
    nominal: 7.5
    tolerance: 0.1
  body_size_y:
    minimum: 17.7
    maximum: 18.1
  overall_height:
    minimum: 2.35
    maximum: 2.65

  overall_size_x: # from JEDEC
    nominal: 10.3
    tolerance: 0.33
  lead_len:
    minimum: 0.4
    maximum: 1.27
  lead_width: # from JEDEC (agrees with linked datasheet)
    minimum: 0.31
    maximum: 0.51

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 14

SOIC-28W_7.5x18.7mm_P1.27mm:
  #round to two significant digits to comply with old name
  custom_name_format: SOIC-{pincount}W_{size_x:.2g}x{size_y:.3g}mm_P{pitch:g}mm
  size_source: 'https://www.akm.com/akm/en/file/datasheet/AK5394AVS.pdf#page=23'
  body_size_x:
    nominal: 7.5
    tolerance: 0.2
  body_size_y:
    nominal: 18.7
    tolerance: 0.3
  overall_height:
    nominal: 2.2
    tolerance: 0.1

  overall_size_x:
    nominal: 10.4
    tolerance: 0.3
  lead_len:
    nominal: 0.75
    tolerance: 0.2
  lead_width:
    nominal: 0.4
    tolerance: 0.1

  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 14

SOIC-32_7.518x20.777mm_P1.27mm:
  # .300" width body, .818" lenght, .050" pitch
  size_source: 'JEDEC MO-119-B, 300 mils width, variation AC, https://www.jedec.org/system/files/docs/Mo-119b.pdf'
  # second source: https://www.mouser.com/ds/2/100/STK14C88-3_001-50592-356490.pdf
  body_size_x:
    minimum: 7.417
    nominal: 7.518
    maximum: 7.595
  body_size_y:
    minimum: 20.625
    nominal: 20.777
    maximum: 20.930
  overall_height:
    maximum: 2.6416
  overall_size_x:
    minimum: 10.287
    nominal: 10.465
    maximum: 10.643
  lead_len:
    minimum: 0.533
    nominal: 0.787
    maximum: 1.041
  lead_width:
    minimum: 0.355
    nominal: 0.406
    maximum: 0.508
  pitch: 1.27
  num_pins_x: 0
  num_pins_y: 16
